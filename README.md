# Security Research

- Cybersecurity is relevant to all people in the modern world, including a strong password policy to protect your emails or to businesses and other organisations needing to protect both devices and data from damages.

- As a start taking different security courses on [tryhackme](https://tryhackme.com) website.

### Penetration Testing

- A Penetration test or pentest is an ethically-driven attempt to test and analyse the security defences to protect these assets and pieces of information. OR. Penetration test is an authorised audit of a computer system's security and defences as agreed by the owners of the systems.

### Hackers are sorted into three hats

- White Hat : A white hat is an ethical computer hacker, or a computer security expert, who specializes in penetration testing and other testing methodologies that ensure the security of an organization's information systems.

- Grey Hat : A grey hat is a computer hacker or computer security expert who may sometimes violate laws or typical ethical standards, but does not have the malicious intent typical of a black hat hacker.

- Black Hat : A black hat hacker is a hacker who violates computer security for their own personal profit or out of malice.

### Rules of Engagement (ROE)

- The ROE is established before the start of a security test, and gives the test team authority to conduct defined activities without the need for additional permissions.
	- [Example ROE document](https://sansorg.egnyte.com/dl/bF4I3yCcnt/?)

| Section      | Description |
| ----------- | ----------- |
| Permission      | This section of the document gives explicit permission for the engagement to be carried out. This permission is essential to legally protect individuals and organisations for the activities they carry out.       |
| Test Scope   | This section of the document will annotate specific targets to which the engagement should apply. For example, the penetration test may only apply to certain servers or applications but not the entire network.        |
| Rules   | The rules section will define exactly the techniques that are permitted during the engagement. For example, the rules may specifically state that techniques such as phishing attacks are prohibited, but MITM (Man-in-the-Middle) attacks are okay.        |


### Testing Methodologies

- The steps a penetration tester takes during an engagement is known as the methodology.

- General theme 

| Stage      | Description |
| ----------- | ----------- |
| Reconnaissance (Information Gathering) | This stage involves collecting as much publically accessible information about a target/organisation as possible, for example, OSINT and research.  |
| Scanning & Enumeration | This stage involves discovering applications and services running on the systems. For example, finding a web server that may be potentially vulnerable. |
| Gaining Access (Exploitation) | This stage involves leveraging vulnerabilities discovered on a system or application. This stage can involve the use of public exploits or exploiting application logic. |
| Privilege Escalation | Once you have successfully exploited a system or application (known as a foothold), this stage is the attempt to expand your access to a system. You can escalate horizontally and vertically, where horizontally is accessing another account of the same permission group (i.e. another user), whereas vertically is that of another permission group (i.e. an administrator). |
| Post-exploitation | This stage involves a few sub-stages: 1. What other hosts can be targeted (pivoting) 2. What additional information can we gather from the host now that we are a privileged user 3.  Covering your tracks 4. Reporting |

#### Known Methodologies

- OSSTMM
	- [The Open Source Security Testing Methodology Manual](https://www.isecom.org/OSSTMM.3.pdf) provides a detailed framework of testing strategies for systems, software, applications, communications and the human aspect of cybersecurity.
		    1. Telecommunications (phones, VoIP, etc.)
    		2. Wired Networks
    		3. Wireless communications

- OWASP
	- The "[Open Web Application Security Project](https://owasp.org/)" framework is a community-driven and frequently updated framework used solely to test the security of web applications and services.

- NIST Cybersecurity Framework 1.1
	- The [NIST Cybersecurity Framework](https://www.nist.gov/cyberframework) is a popular framework used to improve an organisations cybersecurity standards and manage the risk of cyber threats. This framework is a bit of an honourable mention because of its popularity and detail. The framework provides guidelines on security controls & benchmarks for success for organisations from critical infrastructure (power plants, etc.) all through to commercial.  There is a limited section on a standard guideline for the methodology a penetration tester should take.

- NCSC CAF
	- The [Cyber Assessment Framework](https://www.ncsc.gov.uk/collection/caf/caf-principles-and-guidance) (CAF) is an extensive framework of fourteen principles used to assess the risk of various cyber threats and an organisation's defences against these.


### Black box, White box, Grey box Penetration Testing

- Black-Box Testing : This testing process is a high-level process where the tester is not given any information about the inner workings of the application or service. The tester acts as a regular user testing the functionality and interaction of the application or piece of software. This testing can involve interacting with the interface, i.e. buttons, and testing to see whether the intended result is returned. No knowledge of programming or understanding of the programme is necessary for this type of testing.

- Grey-Box Testing : This testing process is the most popular for things such as penetration testing. It is a combination of both black-box and white-box testing processes. The tester will have some limited knowledge of the internal components of the application or piece of software. Still, it will be interacting with the application as if it were a black-box scenario and then using their knowledge of the application to try and resolve issues as they find them.

- White-Box Testing : This testing process is a low-level process usually done by a software developer who knows programming and application logic. The tester will be testing the internal components of the application or piece of software and, for example, ensuring that specific functions work correctly and within a reasonable amount of time. 

### The CIA Triad 

- Confidentiality, Integrity and Availability

- Confidentiality
	- The protection of data from unauthorized access and misuse. Organisations will always have some form of sensitive data stored on their systems. To provide confidentiality is to protect this data from parties that it is not intended for.

- Integrity
	- The CIA triad element of integrity is the condition where information is kept accurate and consistent unless authorized changes are made. It is possible for the information to change because of careless access and use, errors in the information system, or unauthorized access and use. In the CIA triad, integrity is maintained when the information remains unchanged during storage, transmission, and usage not involving modification to the information. Steps must be taken to ensure data cannot be altered by unauthorised people.

- Availability
	- In order for data to be useful, it must be available and accessible by the user.

### Threat Modelling & Incident Response 

- Threat modelling is the process of reviewing, improving, and testing the security protocols in place in an organisation's information technology infrastructure and services.

	- A critical stage of the threat modelling process is identifying likely threats that an application or system may face, the vulnerabilities a system or application may be vulnerable to.

- An effective threat model includes:

    - Threat intelligence
    - Asset identification
    - Mitigation capabilities
    - Risk assessment

- A breach of security is known as an incident. Actions taken to resolve and remediate the threat are known as Incident Response (IR).

- To successfully solve an incident, these steps are often referred to as the six phases of Incident Response that takes place, listed in the table below:

| Action      | Description |
| ----------- | ----------- |
| Preparation      | Do we have the resources and plans in place to deal with the security incident? |
| Identification      | Has the threat and the threat actor been correctly identified in order for us to respond to? |
| Containment      | Can the threat/security incident be contained to prevent other systems or users from being impacted? |
| Eradication      | Remove the active threat. |
| Recovery      | Perform a full review of the impacted systems to return to business as usual operations. |
| Lessons Learned      | What can be learnt from the incident? I.e. if it was due to a phishing email, employees should be trained better to detect phishing emails. |

### Content Discovery

- Firstly, we should ask, in the context of web application security, what is content? Content can be many things, a file, video, picture, backup, a website feature. When we talk about content discovery, we're not talking about the obvious things we can see on a website; it's the things that aren't immediately presented to us and that weren't always intended for public access.

- There are three main ways of discovering content on a website. 
	- Manually
	- Automated and 
	- OSINT (Open-Source Intelligence).


- Manual Discovery
	- robots.txt
		- The robots.txt file is a document that tells search engines which pages they are and aren't allowed to show on their search engine results or ban specific search engines from crawling the website altogether. It can be common practice to restrict certain website areas so they aren't displayed in search engine results. These pages may be areas such as administration portals or files meant for the website's customers. This file gives us a great list of locations on the website that the owners don't want us to discover as penetration testers.

	- Favicon
		- The favicon is a small icon displayed in the browser's address bar or tab used for branding a website.
		- Sometimes when frameworks are used to build a website, a favicon that is part of the installation gets leftover, and if the website developer doesn't replace this with a custom one, this can give us a clue on what framework is in use. OWASP host a database of common framework icons that you can use to check against the targets favicon https://wiki.owasp.org/index.php/OWASP_favicon_database. Once we know the framework stack, we can use external resources to discover more about it
		- `curl https://some-website.com/favicon.ico | md5sum`

	- sitemap.xml
		- Unlike the robots.txt file, which restricts what search engine crawlers can look at, the sitemap.xml file gives a list of every file the website owner wishes to be listed on a search engine. These can sometimes contain areas of the website that are a bit more difficult to navigate to or even list some old webpages that the current site no longer uses but are still working behind the scenes.

	- HTTP Headers 
		- When we make requests to the web server, the server returns various HTTP headers. These headers can sometimes contain useful information such as the webserver software and possibly the programming/scripting language in use.
		- `curl http://website.com -v`
		- `curl -I -XHEAD http://website.com`

	- Framework Stack
		- Once you've established the framework of a website, either from the above favicon example or by looking for clues in the page source such as comments, copyright notices or credits, you can then locate the framework's website. From there, we can learn more about the software and other information, possibly leading to more content we can discover.
- OSINT
	- There are external resources available that can help in discovering information about your target website; these resources are often referred to as OSINT or (Open-Source Intelligence) as they're freely available tools which collect information

	- Google Hacking / Dorking 
		- Google hacking / Dorking utilizes Google's advanced search engine features, which allow you to pick out custom content.

| Filter | Example | Description |
| :--------- | :----------: |-----------: |
| site | site:website.com | returns results only from the specified website address |
| inurl | inurl:admin | returns results that have the specified word in the URL |
| filetype | filetype:pdf | returns results which are a particular file extension |
| intitle | intitle:admin | returns results that contain the specified word in the title |

	- Wappalyzer
		- Wappalyzer (https://www.wappalyzer.com/) is an online tool and browser extension that helps identify what technologies a website uses, such as frameworks, Content Management Systems (CMS), payment processors and much more, and it can even find version numbers as well.

	- Wayback Machine
		- The Wayback Machine (https://archive.org/web/) is a historical archive of websites that dates back to the late 90s. You can search a domain name, and it will show you all the times the service scraped the web page and saved the contents. This service can help uncover old pages that may still be active on the current website.

	- GitHub/GitLab
		- Git is a version control system that tracks changes to files in a project. Working in a team is easier because you can see what each team member is editing and what changes they made to files. When users have finished making their changes, they commit them with a message and then push them back to a central location (repository) for the other users to then pull those changes to their local machines. GitHub is a hosted version of Git on the internet. Repositories can either be set to public or private and have various access controls. You can use GitHub's search feature to look for company names or website names to try and locate repositories belonging to your target. Once discovered, you may have access to source code, passwords or other content that you hadn't yet found.

	- S3 Buckets
		- S3 Buckets are a storage service provided by Amazon AWS, allowing people to save files and even static website content in the cloud accessible over HTTP and HTTPS. The owner of the files can set access permissions to either make files public, private and even writable. Sometimes these access permissions are incorrectly set and inadvertently allow access to files that shouldn't be available to the public. The format of the S3 buckets is http(s)://{name}.s3.amazonaws.com where {name} is decided by the owner. S3 buckets can be discovered in many ways, such as finding the URLs in the website's page source, GitHub repositories, or even automating the process. One common automation method is by using the company name followed by common terms such as {name}-assets, {name}-www, {name}-public, {name}-private, etc.
- Automated Discovery 
	- Automated discovery is the process of using tools to discover content rather than doing it manually. This process is automated as it usually contains hundreds, thousands or even millions of requests to a web server. These requests check whether a file or directory exists on a website, giving us access to resources we didn't previously know existed. This process is made possible by using a resource called wordlists.

	- Common Automation Tools
		- ffuf, dirb and gobuster

### Subdomain Enumeration

- Subdomain enumeration is the process of finding valid subdomains for a domain, but why do we do this? We do this to expand our attack surface to try and discover more potential points of vulnerability.

- Some subdomain enumeration methods
	- Brute Force, OSINT (Open-Source Intelligence) and Virtual Host.
- OSINT
	- SSL/TLS Certificates
		- When an SSL/TLS (Secure Sockets Layer/Transport Layer Security) certificate is created for a domain by a CA (Certificate Authority), CA's take part in what's called "Certificate Transparency (CT) logs". These are publicly accessible logs of every SSL/TLS certificate created for a domain name. The purpose of Certificate Transparency logs is to stop malicious and accidentally made certificates from being used. We can use this service to our advantage to discover subdomains belonging to a domain, sites like https://crt.sh and https://transparencyreport.google.com/https/certificates offer a searchable database of certificates that shows current and historical results.
	- Search Engines
		- Search engines contain trillions of links to more than a billion websites, which can be an excellent resource for finding new subdomains. Using advanced search methods on websites like Google, such as the site: filter, can narrow the search results. For example, `-site:www.domain.com site:*.domain.com` would only contain results leading to the domain name domain.com but exclude any links to www.domain.com; therefore, it shows us only subdomain names belonging to domain.com.
	- Sublist3r
		- To speed up the process of OSINT subdomain discovery, we can automate the above methods with the help of tools like Sublist3r.

- Brute Force
	- DNS Bruteforce 
		- Bruteforce DNS (Domain Name System) enumeration is the method of trying tens, hundreds, thousands or even millions of different possible subdomains from a pre-defined list of commonly used subdomains. Because this method requires many requests, we automate it with tools to make the process quicker. 

			- Common DNS Bruteforce Tools
				- dnsrecon `dnsrecon -t brt -d acmeitsupport.thm`

- Virtual Hosts 
	- Like DNS Bruteforce, we can automate this process by using a wordlist of commonly used subdomains.
		- `ffuf -w /usr/share/wordlists/SecLists/Discovery/DNS/namelist.txt -H "Host: FUZZ.website.com"-fs {size}`


### Authentication Bypass

- Authentication bypass vulnerabilities can be some of the most critical as it often ends in leaks of customers personal data

- Username Enumeration 
	- A helpful exercise to complete when trying to find authentication vulnerabilities is creating a list of valid usernames, which we'll use later in other tasks.
	- Website error messages are great resources for collating this information to build our list of valid usernames. Let's say, we have a form to create a new user account if we go to the  website (http://website.com/customers/signup) signup page.
	- If you try entering the username admin and fill in the other form fields with fake information, you'll see we get the error An account with this username already exists. We can use the existence of this error message to produce a list of valid usernames already signed up on the system by using the ffuf tool.
	- The ffuf tool uses a list of commonly used usernames to check against for any matches.
		- `user@tryhackme$ ffuf -w /usr/share/wordlists/SecLists/Usernames/Names/names.txt -X POST -d "username=FUZZ&email=x&password=x&cpassword=x" -H "Content-Type: application/x-www-form-urlencoded" -u http:website.com/customers/signup -mr "username already exists"`
		- In the above example, the -w argument selects the file's location on the computer that contains the list of usernames that we're going to check exists. The -X argument specifies the request method, this will be a GET request by default, but it is a POST request in our example. The -d argument specifies the data that we are going to send. In our example, we have the fields username, email, password and cpassword. We've set the value of the username to FUZZ. In the ffuf tool, the FUZZ keyword signifies where the contents from our wordlist will be inserted in the request. The -H argument is used for adding additional headers to the request. In this instance, we're setting the Content-Type to the webserver knows we are sending form data. The -u argument specifies the URL we are making the request to, and finally, the -mr argument is the text on the page we are looking for to validate we've found a valid username

- Brute Force
	- A brute force attack is an automated process that tries a list of commonly used passwords against either a single username or, like in our case, a list of usernames.
		- `ffuf -w valid_usernames.txt:W1,/usr/share/wordlists/SecLists/Passwords/Common-Credentials/10-million-password-list-top-100.txt:W2 -X POST -d "username=W1&password=W2" -H "Content-Type: application/x-www-form-urlencoded" -u http://website.com/customers/login -fc 200`
		- This ffuf command is a little different to the previous one in Task 2. Previously we used the FUZZ keyword to select where in the request the data from the wordlists would be inserted, but because we're using multiple wordlists, we have to specify our own FUZZ keyword. In this instance, we've chosen W1 for our list of valid usernames and W2 for the list of passwords we will try. The multiple wordlists are again specified with the -w argument but separated with a comma.  For a positive match, we're using the -fc argument to check for an HTTP status code other than 200.

- Logic Flaw 
	- Sometimes authentication processes contain logic flaws. A logic flaw is when the typical logical path of an application is either bypassed, circumvented or manipulated by a hacker. Logic flaws can exist in any area of a website, but we're going to concentrate on examples relating to authentication in this instance.
	- Logic Flaw Example
		- The below mock code example checks to see whether the start of the path the client is visiting begins with /admin and if so, then further checks are made to see whether the client is, in fact, an admin. If the page doesn't begin with /admin, the page is shown to the client.
		- `if( url.substr(0,6) === '/admin') {
 		    	# Code to check user is an admin
		   } else {
    			# View Page
		   }`
		- Because the above PHP code example uses three equals signs (===), it's looking for an exact match on the string, including the same letter casing. The code presents a logic flaw because an unauthenticated user requesting /adMin will not have their privileges checked and have the page displayed to them, totally bypassing the authentication checks.
		
		- Logic Flaw Practical ![Logic Flaw Practical](./img/Authentication-Bypass.png "Logic Flaw Practical")

- Cookie Tampering 
	- Examining and editing the cookies set by the web server during your online session can have multiple outcomes, such as unauthenticated access, access to another user's account, or elevated privileges.

	- Plain Text
		- The contents of some cookies can be in plain text, and it is obvious what they do. Take, for example, if these were the cookie set after a successful login:
			- Set-Cookie: logged_in=true; Max-Age=3600; Path=/
			- Set-Cookie: admin=false; Max-Age=3600; Path=/
		- We see one cookie (logged_in), which appears to control whether the user is currently logged in or not, and another (admin), which controls whether the visitor has admin privileges. Using this logic, if we were to change the contents of the cookies and make a request we'll be able to change our privileges.
		
		- First, we'll start just by requesting the target page:
			- curl http://website.com/cookie-test
		- We can see we are returned a message of: Not Logged In
		- Now we'll send another request with the logged_in cookie set to true and the admin cookie set to false:
			- curl -H "Cookie: logged_in=true; admin=false" http://website.com/cookie-test
		- We are given the message: Logged In As A User
		- Finally, we'll send one last request setting both the logged_in and admin cookie to true:
			- curl -H "Cookie: logged_in=true; admin=true" http://website.com/cookie-test

	- Hashing
		- Sometimes cookie values can look like a long string of random characters; these are called hashes which are an irreversible representation of the original text. Here are some examples that you may come across:

| Original String |  Hash Method | Output |
| :-------- | :-------: | ------: |
| 1 | md5 | c4ca4238a0b923820dcc509a6f75849b |
| 1 | sha-256 | 6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b |
| 1 | sha-512 | 4dff4ea340f0a823f15d3f4f01ab62eae0e5da579ccb851f8db9dfe84c58b2b37b89903a740e1ee172da793a6e79d560e5f7f9bd058a12a280433ed6fa46510a |
| 1 | sha1 | 356a192b7913b04c54574d18c28d46e6395428ab |
